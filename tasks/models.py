from django.db import models

class TodoItem(models.Model):
    title = models.CharField(max_length=255)
    due_date = models.DateTimeField(null=True, blank=True)
    done = models.BooleanField()

    def __str__(self):
        return self.title
